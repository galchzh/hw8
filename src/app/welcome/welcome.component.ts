import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { TodosService } from '../todos.service';


@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  todos = [ ];
  text:string;
  ifStatus:boolean; //בדיקה מה הסטטוס שבוחרים בפילטר
  todoStatus:boolean; // סטטוס של טודו חדש שמכניסים
  key;
  taskStatus = [];
 

  showTodo()
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/' + user.uid + '/todos').snapshotChanges().subscribe(
        todos => {
          this.todos = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
            }
          )
        }  
      )
    })
  }


  toFilter()
  {
    this.authService.user.subscribe(user => {
    this.db.list('/users/uid/'+user.uid+'/todos').snapshotChanges().subscribe(
      todos =>{
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;          
              if (this.ifStatus == y['status']) {
                this.todos.push(y);
              }
              else if (this.ifStatus != true && this.ifStatus != false)
              {
                this.todos.push(y);
              }
              
          }
        )
      }
    )
    })
  }
 
 
  addTodo(){
    this.todosService.addTodo(this.text, this.todoStatus);
    this.text = '';
    this.todoStatus = false;
    
  }

  
  constructor(private router:Router,
              private authService:AuthService,
              private db:AngularFireDatabase,
              private todosService:TodosService,
             ) 
              { 
                this.showTodo();
            }
          
  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{
          this.todos = [];
          this.taskStatus = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
              let sta = y['status'];
              if (this.taskStatus.indexOf(sta)== -1)
               {
               this.taskStatus.push(sta);
              }
             
            }
          )
        }
      ) 
    })
  }

}